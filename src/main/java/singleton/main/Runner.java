package singleton.main;

import singleton.superman.AtomicSuperman;
import singleton.superman.Superman;

public class Runner {
	public static void main(String[] args) {
		Superman superman = Superman.getInstance();
		AtomicSuperman atomicSuperman = AtomicSuperman.getInstance();
	}
}
