package singleton.superman;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AtomicSuperman {

	private static AtomicSuperman instance;
	private static AtomicBoolean isNull = new AtomicBoolean(true);
	private static Lock lock = new ReentrantLock();

	private AtomicSuperman() {

	}

	public static AtomicSuperman getInstance() {
		if (isNull.get()) {
			lock.lock();
			try {
				if (isNull.get()) {
					instance = new AtomicSuperman();
					isNull.set(false);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}
}
