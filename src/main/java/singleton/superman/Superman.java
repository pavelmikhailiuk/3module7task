package singleton.superman;

public class Superman {

	private Superman() {

	}

	private static class SupermanHelper {
		private static final Superman INSTANCE = new Superman();
	}

	public static Superman getInstance() {
		return SupermanHelper.INSTANCE;
	}
}
